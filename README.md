# mkk-emulator

Emulator for MKK-1, my custom computer (non physical sadly). It's atm 8-bit (so i can fit all uppercase characters in).

### Why

So I saw people making again advanced redstone computers in mainkraft, so I decided to make my own. But building all the modules and optionally cloning them with mcedit would take sooo much time. So I decided to write an emulator until it's finished in my favourite programming language - Java (sorry suckless, but it's still better than rust and haskell).

### Roadmap

[X] 4 to 8 bit transition

[X] working executables

[X] working Assembly

[ ] a hight-level language

### Assembly Hello World:

    PRINT HELLO 0 ; print hello
    SET CHAR NL
    CALL DISP PRINT ; print a newline character
    PRINT WORLD 0
    SET CHAR NL
    CALL DISP PRINT
    CLM 0 0 ; clean the memory

After that, compile it with:

    java -jar mkk-emulator-x.x.jar -c /path/to/SOURCE.ASM /path/to/OUTPUT.CC

Or within the Shell (-s):

    ASM /path/to/SOURCE.ASM /path/to/OUTPUC.CC

And run:

    java -jar mkk-emulator-x.x.jar -b /path/to/OUTPUT.CC

### Questions

But .CC is the C++ extension

    no i can assign then whatever i want.
    and 'CC' stands for 'Compiled Contraption'.