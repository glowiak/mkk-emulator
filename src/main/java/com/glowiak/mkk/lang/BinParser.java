package com.glowiak.mkk.lang;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileWriter;
import java.io.File;

interface BINARY_KEYWORDS
{
    public static final String SET = "00000001"; // set
    public static final String CALL = "00000010"; // call
    public static final String ADD = "00000011"; // add
    public static final String DEC = "00000100"; // subtract
    public static final String CLM = "00000101"; // clear registers
    public static final String LOAD = "00000110"; // load
    public static final String IF = "00000111"; // if
    public static final String DRAW = "00001000"; // draw
    public static final String SYS = "00001001"; // execute another program
    public static final String MOV = "00001010"; // move value from one register to another
    public static final String PREG = "00001011"; // print value of a register
}
interface BINARY_LETTERS
{
    public static final String A = "00000001";
    public static final String B = "00000010";
    public static final String C = "00000011";
    public static final String D = "00001100";
    public static final String E = "00000101";
    public static final String F = "00001110"; //
    public static final String G = "00001111"; //
    public static final String H = "00000100";
    public static final String I = "00010000"; //
    public static final String O = "00000110";
    public static final String L = "00000111";
    public static final String W = "00001000";
    public static final String N = "00001001";
    public static final String U = "00001010";
    public static final String R = "00001011";
    public static final String NL = "00001101";
}

public class BinParser implements BINARY_KEYWORDS, BINARY_LETTERS
{
    public static int REGISTER[] = new int[16];
    public static int CALLS[] = new int[16];
    public static char REGCHAR;
    
    public static void EXEC_BIN(String path)
    {
        try {
            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);
            
            String s = null;
            while ((s = br.readLine()) != null)
            {
                String[] ALL = s.split("\\s+");
                
                String COMMAND = ALL[0];
                String REG = ALL[1];
                String VAL = ALL[2];
                
                switch(COMMAND)
                {
                    case SET:
                        if (Integer.parseInt(REG, 2) == 7)
                        { REGCHAR = binToChar(VAL); } else
                        { REGISTER[Integer.parseInt(REG, 2)] = Integer.parseInt(VAL, 2); }
                        break;
                    case ADD:
                        REGISTER[Integer.parseInt(REG, 2)] += Integer.parseInt(VAL, 2);
                        break;
                    case DEC:
                        REGISTER[Integer.parseInt(REG, 2)] -= Integer.parseInt(VAL, 2);
                        break;
                    case CLM:
                        for (int i = 0; i < 16; i++)
                        {
                            REGISTER[i] = 0;
                        }
                        break;
                    case CALL:
                        if (Integer.parseInt(REG, 2) == 1) // screen
                        {
                            if (Integer.parseInt(VAL, 2) == 1) // print
                            {
                                System.out.print(String.format("%c", REGCHAR));
                            }
                            if (Integer.parseInt(VAL, 2) == 2) // clear
                            {
                                System.out.print("\033[H\033[2J");
                            }
                            if (Integer.parseInt(VAL, 2) == 3) // newline
                            {
                                System.out.print("\n");
                            }
                            if (Integer.parseInt(VAL, 2) == 4) // space
                            {
                                System.out.print(" ");
                            }
                        }
                        break;
                    case SYS:
                        if (new File(VAL).exists()) { EXEC_BIN(VAL); }
                        if (new File(String.format("%s.CC", VAL)).exists()) { EXEC_BIN(String.format("%s.CC", VAL)); }
                        if (new File(String.format("%s.cc", VAL)).exists()) { EXEC_BIN(String.format("%s.cc", VAL)); }
                        break;
                    case MOV:
                        REGISTER[Integer.parseInt(VAL, 2)] = REGISTER[Integer.parseInt(REG, 2)];
                        REGISTER[Integer.parseInt(REG, 2)] = 0;
                        break;
                    case PREG:
                        if (REG == "CHAR")
                        {
                            System.out.print(String.format("%c", REGCHAR));
                        } else
                        {
                            System.out.print(REGISTER[Integer.parseInt(REG, 2)]);
                        }
                        break;
                }
            }
            
            br.close();
            fr.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static char binToChar(String bin)
    {
        char w = ' ';
        switch(bin)
        {
            case A: w = 'A'; break;
            case B: w =  'B'; break;
            case C: w = 'C'; break;
            case H: w = 'H'; break;
            case E: w = 'E'; break;
            case O: w = 'O'; break;
            case L: w = 'L'; break;
            case W: w = 'W'; break;
            case N: w = 'N'; break;
            case U: w = 'U'; break;
            case R: w = 'R'; break;
            case D: w = 'D'; break;
            case NL: w = '\n'; break;
        }
        return w;
    }
    public static int binToDec(String bin)
    {
        int w = 0;
        switch(bin)
        {
            case "00000000": w = 0; break;
            case "00000001": w = 1; break;
            case "00000010": w = 2; break;
            case "00000011": w = 3; break;
            case "00000100": w = 4; break;
            case "00000101": w = 5; break;
            case "00000110": w = 6; break;
            case "00000111": w = 7; break;
            case "00001000": w = 8; break;
            
            case "00001001": w = 9; break;
            case "00001010": w = 10; break;
            case "00001011": w = 11; break;
            case "00001100": w = 12; break;
            case "00001101": w = 13; break;
            case "00001110": w = 14; break;
            case "00001111": w = 15; break;
        }
        return w;
    }
}
