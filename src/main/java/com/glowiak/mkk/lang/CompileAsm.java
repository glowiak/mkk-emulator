package com.glowiak.mkk.lang;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;

/*    public static final String A = "00000001";
*    public static final String B = "00000010";
*    public static final String C = "00000011";
*    public static final String H = "00000100";
*    public static final String E = "00000101";
*    public static final String O = "00000110";
*    public static final String L = "00000111";
*    public static final String W = "00001000";
*    public static final String N = "00001001";
*    public static final String U = "00001010";
*    public static final String R = "00001011";
*    public static final String D = "00001100";
*    public static final String NL = "00001101";
*/

public class CompileAsm implements BINARY_KEYWORDS, BINARY_LETTERS
{
    public static void ASM(String src, String dst)
    {
        try {
            FileReader fr = new FileReader(src);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter(dst);
            
            String s = null;
            while ((s = br.readLine()) != null)
            {
                String[] ALL = s.split("\\s+");
                
                String COMMAND = ALL[0];
                String REG = ALL[1];
                String VAL = ALL[2];
                
                switch(COMMAND)
                {
                    case "SET":
                        fw.write(String.format("00000001 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "ADD":
                        fw.write(String.format("00000011 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "CALL":
                        fw.write(String.format("00000010 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "DEC":
                        fw.write(String.format("00000100 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "CLM":
                        fw.write(String.format("00000101 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "LOAD":
                        fw.write(String.format("00000110 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "IF":
                        fw.write(String.format("00000111 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "DRAW":
                        fw.write(String.format("00001000 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "SYS":
                        fw.write(String.format("00001001 00000000 %s\n", REG));
                        break;
                    case "MOV":
                        fw.write(String.format("00001010 %s %s\n", toBinary(REG), toBinary(VAL)));
                        break;
                    case "PRINT":
                        for (int i = 0; i < REG.length(); i++)
                        {
                            fw.write(String.format("00000001 00000111 %s\n", toBinary(String.format("%c", REG.charAt(i)))));
                            fw.write("00000010 00000001 00000001\n");
                        }
                        break;
                }
            }
            
            fw.close();
            br.close();
            fr.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static void DEASM(String src, String dst)
    {
        if (!new File(src).exists())
        {
            System.out.println("?ERROR? FILE DOES NOT EXIST");
            return;
        }
        try {
            FileReader fr = new FileReader(src);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter(dst);
            
            String s = null;
            while ((s = br.readLine()) != null)
            {
                String[] ALL = s.split("\\s+");
                
                String COMMAND = ALL[0];
                String REG = ALL[1];
                String VAL = ALL[2];
                
                switch(COMMAND)
                {
                    case SET:
                        if (Integer.parseInt(REG, 2) == 7)
                        { fw.write(String.format("SET CHAR %s\n", Integer.parseInt(VAL, 2))); } else
                        { fw.write(String.format("SET %s %s\n", Integer.parseInt(REG, 2), Integer.parseInt(VAL, 2))); }
                        break;
                    case ADD:
                        fw.write(String.format("ADD %s %s\n", Integer.parseInt(REG, 2), Integer.parseInt(VAL, 2)));
                        break;
                    case DEC:
                        fw.write(String.format("DEC %s %s\n", Integer.parseInt(REG, 2), Integer.parseInt(VAL, 2)));
                        break;
                    case CLM:
                        fw.write("CLM 0 0\n");
                        break;
                    case CALL:
                        if (Integer.parseInt(REG, 2) == 1)
                        {
                            if (Integer.parseInt(VAL, 2) == 1)
                            {
                                fw.write("CALL DISP PRINT\n");
                            }
                            if (Integer.parseInt(VAL, 2) == 2)
                            {
                                fw.write("CALL DISP CLR\n");
                            }
                        }
                        break;
                    case SYS:
                        fw.write(String.format("SYS %s 0\n", REG));
                        break;
                    case MOV:
                        fw.write(String.format("MOV %s %s\n", Integer.parseInt(REG, 2), Integer.parseInt(VAL, 2)));
                        break;
                }
            }
            
            br.close();
            fr.close();
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static String toBinary(String value)
    {
        String w = "";
        switch(value)
        {
            case "DISP": w = "00000001"; break;
            case "CHAR": w = "00000111"; break;
            case "PRINT": w = "00000001"; break;
            case "CLEAR": w = "00000001"; break;
            case "LINE": w = "00001000"; break;
            case "CLR": w = "00000010"; break;
            case "NEWL": w = "00000011"; break;
            case "SPACE": w = "00000100"; break;
            
            case "A": w = "00000001"; break;
            case "B": w = "00000010"; break;
            case "C": w = "00000011"; break;
            case "H": w = "00000100"; break;
            case "E": w = "00000101"; break;
            case "O": w = "00000110"; break;
            case "L": w = "00000111"; break;
            case "W": w = "00001000"; break;
            case "N": w = "00001001"; break;
            case "U": w = "00001010"; break;
            case "R": w = "00001011"; break;
            case "D": w = "00001100"; break;
            case "NL": w = "00001101"; break;
            
            case "0": w = "00000000"; break;
            case "1": w = "00000001"; break;
            case "2": w = "00000010"; break;
            case "3": w = "00000011"; break;
            case "4": w = "00000100"; break;
            case "5": w = "00000101"; break;
            case "6": w = "00000110"; break;
            case "7": w = "00000111"; break;
            case "8": w = "00001000"; break;
            case "9": w = "00001001"; break;
            case "10": w = "00001010"; break;
            case "11": w = "00001011"; break;
            case "12": w = "00001100"; break;
            case "13": w = "00001101"; break;
            case "14": w = "00001110"; break;
            case "15": w = "00001111"; break;
        }
        return w;
    }
}
