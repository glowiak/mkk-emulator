package com.glowiak.mkk.lang;

import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import java.util.Scanner;

import com.glowiak.mkk.lang.CompileAsm;
import com.glowiak.mkk.lang.BinParser;

public class Shell
{
    public static void shell()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("MKK-1 SHELL V1.0");
        
        while (true)
        {
            System.out.print("> ");
            String v1 = scanner.nextLine();
            
            String[] ALL = v1.split("\\s+");
            
            if (new File(v1).exists()) { BinParser.EXEC_BIN(v1); }
            if (new File(String.format("%s.CC", v1)).exists()) { BinParser.EXEC_BIN(String.format("%s.CC", v1)); }
            if (new File(String.format("%s.cc", v1)).exists()) { BinParser.EXEC_BIN(String.format("%s.cc", v1)); }
            
            if (ALL[0].equals("exit") || ALL[0].equals("EXIT")) { System.exit(0); }
            if (ALL[0].equals("asm") || ALL[0].equals("ASM"))
            {
                if (ALL.length < 3)
                {
                    System.out.println("?ERROR? NOT ENOUGH ARGUMENTS");
                } else
                {
                    String F_SRC = ALL[1];
                    String F_OUT = ALL[2];
                
                    if (!new File(F_SRC).exists())
                    {
                        System.out.println("?ERROR? FILE CANNOT BE FOUND!");
                    } else
                    {
                        System.out.println("MKK-1 ASSEMBLER v0.1");
                        CompileAsm.ASM(F_SRC, F_OUT);
                        System.out.println(String.format("%s COMPILED TO %s", F_SRC, F_OUT));
                    }
                }
            }
            if (ALL[0].equals("deasm") || ALL[0].equals("DEASM"))
            {
                if (ALL.length < 3)
                {
                    System.out.println("?ERROR? NOT ENOUGH ARGUMENTS");
                } else
                {
                    String F_SRC = ALL[1];
                    String F_OUT = ALL[2];
                
                    if (!new File(F_SRC).exists())
                    {
                        System.out.println("?ERROR? FILE CANNOT BE FOUND!");
                    } else
                    {
                        System.out.println("MKK-1 DEASSEMBLER v0.1");
                        CompileAsm.DEASM(F_SRC, F_OUT);
                        System.out.println(String.format("%s DECOMPILED TO %s", F_SRC, F_OUT));
                    }
                }
            }
            if (ALL[0].equals("print") || ALL[0].equals("PRINT"))
            {
                System.out.println(ALL[1].toUpperCase());
            }
        }
    }
}
