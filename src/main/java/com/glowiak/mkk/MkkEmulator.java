package com.glowiak.mkk;

import com.glowiak.mkk.lang.BinParser;
import com.glowiak.mkk.lang.CompileAsm;
import com.glowiak.mkk.lang.Shell;
import com.glowiak.mkk.gui.MainWindow;

import java.io.File;

public class MkkEmulator
{
    public static void main(String[] args)
    {
        if (args.length >= 1)
        {
            if (args[0].equals("-b"))
            { BinParser.EXEC_BIN(args[1]); }
            
            if (args[0].equals("-c"))
            {
                CompileAsm.ASM(args[1], args[2]);
            }
            if (args[0].equals("-d"))
            {
                CompileAsm.DEASM(args[1], args[2]);
            }
            if (args[0].equals("-s")) { Shell.shell(); }
            if (args[0].equals("-g")) { MainWindow.run(); }
        } else
        {
            System.out.println("MKK-1 Emulator v0.1 by glowiak");
            System.out.println("--------------");
            System.out.println("Usage: java -jar mkk-emulator.jar $ARG");
            System.out.println("ARGs:");
            System.out.println("-b /path/to/FILE.CC                       | execute binary file");
            System.out.println("-c /path/to/SOURCE.ASM /path/to/OUTPUT.CC | compile assembly");
            System.out.println("-d /path/to/FILE.CC /path/to/OUTPUT.ASM   | decompile into assembly");
            System.out.println("-s                                        | run shell");
            System.out.println("-g                                        | run GUI");
        }
    }
}
