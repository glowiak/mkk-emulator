package com.glowiak.mkk.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;

import com.glowiak.mkk.lang.CompileAsm;
import com.glowiak.mkk.lang.BinParser;

interface MWI
{
    JFrame w = new JFrame("MKK-1 Emulator");
    JTextArea tf_code = new JTextArea();
    JButton b_run = new JButton("Run");
    JButton b_shell = new JButton("Shell");
    JButton b_com = new JButton("Compile");
    JButton b_new = new JButton("New");
    JButton b_open = new JButton("Open");
    JButton b_save = new JButton("Save");
    JRadioButton rb_machine = new JRadioButton("Machine code");
    JRadioButton rb_assembly = new JRadioButton("Assembly");
    
    ButtonGroup bg = new ButtonGroup();
}

public class MainWindow implements MWI
{
    public static boolean saved = false;
    public static String savedFile = "";
    public static void run()
    {
        draw(700, 500);
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        b_new.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent e)
            {
                System.out.println("BLANK");
                tf_code.setText("");
                saved = false;
                savedFile = "";
            }
        });
        b_com.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if (saved == true)
                {
                    JFileChooser jfc = new JFileChooser();
                    int jfr = jfc.showSaveDialog(w);
                    if (jfr == JFileChooser.APPROVE_OPTION)
                    {
                        System.out.println("COMPILE");
                        System.out.println(String.format("> ASM %s %s", savedFile, jfc.getSelectedFile().getPath()));
                        CompileAsm.ASM(savedFile, jfc.getSelectedFile().getPath());
                    }
                } else
                {
                    JOptionPane.showMessageDialog(w, "Save your file first!");
                }
            }
        });
        b_open.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int jfr = jfc.showOpenDialog(w);
                if (jfr == JFileChooser.APPROVE_OPTION)
                {
                    savedFile = jfc.getSelectedFile().getPath();
                    try {
                        FileReader fr = new FileReader(savedFile);
                        BufferedReader br = new BufferedReader(fr);
                        
                        tf_code.setText("");
                        String s = null;
                        while ((s = br.readLine()) != null)
                        {
                            tf_code.append(s);
                            tf_code.append("\n");
                        }
                        
                        br.close();
                        fr.close();
                        saved = true;
                    } catch (IOException ioe) { System.out.println(ioe); }
                }
            }
        });
        b_save.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("SAVE");
                
                if (saved == false)
                {
                    JFileChooser jfc = new JFileChooser();
                    int jfr = jfc.showSaveDialog(w);
                    if (jfr == JFileChooser.APPROVE_OPTION)
                    {
                        try {
                            saved = true;
                            savedFile = jfc.getSelectedFile().getPath();
                            FileWriter fw = new FileWriter(savedFile);
                            
                            fw.write(tf_code.getText());
                            
                            fw.close();
                        } catch (IOException ioe) { System.out.println(ioe); }
                    }
                } else
                {
                    try {
                        FileWriter fw = new FileWriter(savedFile);
                        
                        fw.write(tf_code.getText());
                        
                        fw.close();
                    } catch (IOException ioe) { System.out.println(ioe); }
                }
            }
        });
    }
    public static void draw(int width, int height)
    {
        w.setSize(width, height);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        tf_code.setBounds(150, 0, width - 150, height);
        b_run.setBounds(0, 0, 150, 25);
        b_shell.setBounds(0, 25, 150, 25);
        b_com.setBounds(0, 50, 150, 25);
        b_new.setBounds(0, 75, 150, 25);
        b_open.setBounds(0, 100, 150, 25);
        b_save.setBounds(0, 125, 150, 25);
        
        w.add(tf_code);
        w.add(b_run);
        w.add(b_shell);
        w.add(b_com);
        w.add(b_new);
        w.add(b_open);
        w.add(b_save);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
